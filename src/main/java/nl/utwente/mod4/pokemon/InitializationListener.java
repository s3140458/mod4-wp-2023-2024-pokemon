package nl.utwente.mod4.pokemon;

import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import nl.utwente.mod4.pokemon.dao.PokemonTypeDao;
import nl.utwente.mod4.pokemon.dao.TrainerDao;

import java.io.IOException;

@WebListener
public class InitializationListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("Initializing PokeApp...");
        try {
            PokemonTypeDao.INSTANCE.load();
            TrainerDao.INSTANCE.load();
        } catch (IOException e) {
            System.err.println("Error while loading data.");
            e.printStackTrace();
        }
        System.out.println("PokeApp initialized.");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("Shutting down PokeApp...");
        try {
            PokemonTypeDao.INSTANCE.save();
            TrainerDao.INSTANCE.save();
        } catch (IOException e) {
            System.err.println("Error while saving data.");
            e.printStackTrace();
        }
        System.out.println("PokeApp shutdown.");
    }

}
